+++
title = "Features"
date = 2021-10-23T23:04:51-07:00
weight = 2
+++

The list of features is mentioned [here](https://gitea.arsenm.dev/Arsen6331/itd#features) in the readme. This page will describe how certain features work and how new ones are added.

### Notification Relay

The notification relay works by listening for method calls on the `org.freedesktop.Notifications` DBus interface. Any such method call represents a notification being sent. ITD reads the data from the method call and creates a notification suitable for ANS (Alert Notification Service) which is used by InfniTime to display notifications. This means DBus must be installed and running for ITD to relay notifications.

### Transliteration

Transliteration for ITD is implemented in a custom `translit` package. It consists of a `Transliterator` interface which looks like this:

```go
type Transliterator interface {
	Transliterate(string) string
	Init()
}
```

Anything that satisfies that interface can be added to the `Transliterators` map and then used in ITD's config in the `notifs.translit.use` array.

### Music Control

Music control is implemented using `playerctl` which uses the MPRIS DBus interface. This means it will work with any music player which supports MPRIS. `playerctl` must be installed for ITD to do music control.

### Control Socket

ITD exposes a UNIX socket at `/tmp/itd/socket` by default. This socket can be used to control the daemon. It is how `itctl` and `itgui` work. This can be used in any language which supports UNIX sockets which is nearly all of them. Even bash can be used with `netcat`.

### Weather

ITD uses data from OpenStreetMap Nominatim to get coordinates for a location, and then feeds those to the Norwegian Meteorological Institute's API to retrieve weather for that location.

### New Features

New features are added whenever I find out they exist from InfiniTime's repo or developers. This means ITD's master branch may contain features not yet released in InfiniTime. 