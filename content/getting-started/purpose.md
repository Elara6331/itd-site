+++
title = "Purpose"
date = 2021-10-23T22:34:32-07:00
weight = 1
+++

ITD was created because I could not find a good InfiniTime companion for Linux. There is [Siglo](https://github.com/alexr4535/siglo) which cannot do much beyond syncing time and updating the watch and is not very reliable. There is [Amazfish](https://github.com/piggz/harbour-amazfish) which works, but at least for me, is a bit clunky and unreliable.

I wanted something that was easy enough to use that I could just run and forget about, and had any feature I may want to use. Also, I needed it to work on Linux because I only own Linux devices, including my phone, which is a PinePhone. This leads to the next requirement. I needed it to be easily cross-compiled so that I could use it on all my computers as well as aarch64 devices such as my PinePhone and SBCs. All of these reasons contributed to me deciding to make ITD and they are what I try to emphasize in my development.