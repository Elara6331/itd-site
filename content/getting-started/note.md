+++
title = "Note"
date = 2021-10-23T22:55:22-07:00
weight = 3
+++

This is a note for anyone using or planning to contribute to ITD. I am {{<raw>}}<span id="age">Loading...</span>{{</raw>}} years old and, at times, may be busy with school and unable to answer questions and/or work on the project for a while. If you do not get an answer quickly, I am probably busy. However, I sometimes answer questions, comment on issues, or push code while in school. That means there may be pauses where I cannot answer questions or comment on issues because I am moving between classes or busy doing an assignment. Please do not bombard me with messages if this occurs, I will answer as soon as I can.

{{<raw>}}
<script>
    birthday = new Date("April 24, 2005");
    now =new Date();
    age = now.getFullYear() - birthday.getFullYear();
    if (now.getMonth() < birthday.getMonth()) {
        age--;
    } else if (now.getMonth() == birthday.getMonth() && now.getDate() < birthday.getDate()) {
        age--;
    }
    document.getElementById("age").innerHTML = age;
</script>
{{</raw>}}