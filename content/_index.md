+++
title = "Home"
date = 2021-10-23T00:11:38-07:00
+++

# ITD

ITD is a daemon written in Go that communicates with the [InfiniTime](https://github.com/InfiniTimeOrg/InfiniTime) firmware running on the [PineTime](https://www.pine64.org/pinetime/) smartwatch.

ITD is meant to be fast and easy to use. As such, it is is only a single binary that works on all Linux systems since it is statically compiled and thus does not rely on any dynamic libraries.

ITD comes with `itctl`, a binary that uses the provided socket to control ITD and, by extension, the connected watch.


`itgui` is a gui frontend in ITD's repo. I do not compile it in CI as it requires OpenGL which makes cross-compiling a massive pain.