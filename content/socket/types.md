+++
title = "Types"
date = 2021-10-23T23:04:51-07:00
weight = 1
+++

When making a request to ITD or receiving a response from it, a type integer is used to indicate what kind of request is being made. This page will document the integers and what they mean.

Here is a list of all the request types:

```go
const (
	ReqTypeHeartRate      = 0
	ReqTypeBattLevel      = 1
	ReqTypeFwVersion      = 2
	ReqTypeFwUpgrade      = 3
	ReqTypeBtAddress      = 4
	ReqTypeNotify         = 5
	ReqTypeSetTime        = 6
	ReqTypeWatchHeartRate = 7
	ReqTypeWatchBattLevel = 8
	ReqTypeMotion         = 9
	ReqTypeWatchMotion    = 10
	ReqTypeStepCount      = 11
	ReqTypeWatchStepCount = 12
	ReqTypeCancel         = 13
)
```

The response types are always the same as the request type used.

ITD uses Go's `iota` keyword to generate these numbers, but I have placed their actual values on this page to make it easier to use them in other languages.