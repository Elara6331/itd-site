+++
title = "API"
date = 2021-10-23T23:04:51-07:00
weight = 2
+++

This page documents the API of ITD's control socket.

## Request format

Request sent to ITD's socket should be valid JSON with the following format:

| Field | Type |
|-------|------|
| type  | int  |
| data  | any  |

Example:

```json
{"type": 5, "data": {"title": "title1", "body": "body1"}}
```

Sends notification titled "title1" with a body "body1".

---

## Response format

Responses received from ITD will be valid JSON and have the following format:

| Field  | Type   |
|--------|--------|
| type   | int    |
| value? | any    |
| msg?   | string |
| id?    | string |
| error  | bool   |

(Fields marked with "?" may not exist in all responses)

Examples:

```json
{"type":1,"value":66,"error":false}
```
```json
{"type":6,"msg":"Data required for settime request","error":true}
```
```json
{"type":6,"error":false}
```

---

## Requests

This section will document each request type, its response, and what data it needs.

### Heart Rate

The heart rate request's type is 0. It requires no data and returns a `uint8` in its value field.

Example request:

```json
{"type":0}
```

Example response:

```json
{"type":0,"value":92,"error":false}
```

---

### Battery Level

The battery level request's type is 1. It requires no data and returns a `uint8` in its value field.

Example request:

```json
{"type":1}
```

Example response:

```json
{"type":1,"value":65,"error":false}
```

---

### Firmware Version

The firmware version request's type is 2. It requires no data and returns a string in its value field.

Example request:

```json
{"type":2}
```

Example response:

```json
{"type":2,"value":"1.6.0","error":false}
```

---

### Firmware Upgrade

The firmware upgrade request's type is 3. It requires data in the following format:

| Field | Type     |
|-------|----------|
| type  | int      |
| files | []string |

Example requests:

```json
{"type": 3, "data": {"type": 0, "files": ["pinetime-mcuboot-app-dfu-1.6.0.zip"]}}
```

```json
{"type": 3, "data": {"type": 1, "files": ["pinetime-mcuboot-app-image-1.6.0.bin", "pinetime-mcuboot-app-image-1.6.0.dat"]}}
```

The paths need to be absolute. They are not absolute here as this is an example.

Example response:

```json
{"type":3,"value":{"sent":2800,"recvd":2800,"total":361152},"error":false}
```

Responses will be sent continuously until the transfer is complete.

---

### Bluetooth Address

The bluetooth address request's type is 4. It requires no data and returns a string in its value field.

Example request:

```json
{"type":4}
```

Example response:

```json
{"type":4,"value":"ED:47:AC:47:F4:FB","error":false}
```

---

### Notify

The notify request's type is 5. It reques data in the following format:

| Field | Type   |
|-------|--------|
| title | string |
| body  | string |

Example request:

```json
{"type": 5, "data": {"title": "title1", "body": "body1"}}
```

Example response:

```json
{"type":5,"error":false}
```

---

### Set Time

The set time request's type is 6. It requires a string as data. The string must be a date and time formatted as ISO8601/RFC3339 or the string "now".

Example requests:

```json
{"type":6,"data":"2021-10-24T06:40:35-07:00"}
```
```json
{"type":6,"data":"now"}
```

Example response:

```json
{"type":6,"error":false}
```

---

### Watch Heart Rate

The watch heart rate request's type is 7. It requires no data. It returns a uint8 as its value every time the heart rate changes until it is canceled via the cancel request. It also returns an ID for use with the cancel request.

Example request:

```json
{"type":7}
```

Example response:

```json
{"type":7,"value":83,"id":"d12e2ec2-accd-400c-9da7-be86580b067f","error":false}
```

---

### Watch Battery Level

The watch battery level request's type is 8. It requires no data. It returns a uint8 as its value every time the battery level changes until it is canceled via the cancel request. It also returns an ID for use with the cancel request.

Example request:

```json
{"type":8}
```

Example response:

```json
{"type":8,"value":63,"id":"70cce449-d8b8-4e07-a000-0ca4ee7a9c42","error":false}
```

---

### Motion

The motion request's type is 9. It requires no data. It returns data in the following format:

| Field | Type   |
|-------|--------|
| X     | int16  |
| Y     | int16  |
| Z     | int16  |

The values will only update if the watch is not sleeping.

Example request:

```json
{"type":9}
```

Example response:

```json
{"type":9,"value":{"X":-220,"Y":475,"Z":-893},"error":false}
```

---

### Watch Motion

The watch motion request's type is 10. It requires no data. It returns the same data as the motion request as its value every time the watch moves until it is canceled via the cancel request. It also returns an ID for use with the cancel request.

Example request:

```json
{"type":10}
```

Example response:

```json
{"type":10,"value":{"X":-220,"Y":475,"Z":-893},"id":"d084789d-9fdc-4fce-878b-4408cd616901","error":false}
```

---

### Step Count

The step count request's type is 11. It requires no data and returns a `uint32` in its value field.

Example request:

```json
{"type":11}
```

Example response:

```json
{"type":11,"value":1043,"error":false}
```

---

### Watch Step Count

The watch step count request's type is 12. It requires no data. It returns a `uint32` in its value field every time the step count changes until it is canceled via the cancel request. It also returns an ID for use with the cancel request.

Example request:

```json
{"type":12}
```

Example response:

```json
{"type":12,"value":1045,"id":"54583e8f-80f6-45e3-a97f-b111defc0edc","error":false}
```

---

### Cancel

The cancel request's type is 13. It requires a string as data, containing the ID returned by a watch request. Once run, it will terminate the watch request, clean up anything it was using, and cause ITD to stop listening for its value from the watch.

Example request:

```json
{"type":13,"data":"54583e8f-80f6-45e3-a97f-b111defc0edc"}
```

Example response:

```json
{"type":13,"error":false}
```