+++
title = "About"
date = 2021-10-23T23:04:51-07:00
weight = 1
+++

ITD has a config file. It's a TOML file that is, by default, at `/etc/itd.toml`. ITD will also search `~/.config/itd.toml` for a config and will choose the one in home if possible.

ITD can also be configured with environment variables. They take priority over the config. Variable names should be formatted as follows: ITD_&lt;config field&gt;. For example, socket path is `socket.path` in the config, so it would be `ITD_SOCKET_PATH` as an environment variable. To change which transliterators to use, `ITD_NOTIFS_TRANSLIT_USE` would be the variable, since the config field is `notifs.translit.use`.

In most cases, config edits come into effect immediately, and no restart of ITD is needed.