+++
title = "Sections"
date = 2021-10-23T23:04:51-07:00
weight = 2
+++

This page is about the various sections in the config, their purpose, and how to use them.

### socket

This section contains options for the control socket exposed by ITD.

#### path

This field is a string indicating the filepath at which the control socket should be opened.

---

### conn

The conn secton contains options for ITD's connection to the PineTime. 

#### reconnect

This field is a boolean that dictates whether ITD will attempt to reconnect to the PineTime in case of a disconnect.

---

### conn.whitelist

This section allows filtering which watches will be connected to. Only the devices in the devices list will be connected to.

#### enabled

This field is a boolean which dictates whether the whitelist is enabled.

#### devices

This field is an array if strings which contains BLE addresses that should be allowed for connection. A PineTime's BLE address can be found in the third page of the about section in settings.

---

### on.connect

This section contains options for what happens when ITD first connects to a PineTime.

#### notify

This field is a boolean that dictates whether a notification should be sent to InfiniTime upon connection.

---

### on.reconnect

This section contains options for what happens when ITD reconnects to a PineTime after a disconnect.

#### notify

This field is a boolean that dictates whether a notification should be sent to InfiniTime upon reconnect.

#### setTime

This field is a boolean that dictates whether the time should be updated when ITD reconnects to InfiniTime.

---

### notifs.translit

#### use

This field is an array of strings which sets which transliterators should be used when transliterating text. The full list of transliterators can be found [here](https://gitea.arsenm.dev/Arsen6331/itd#transliteration) in the readme.

#### custom

This field is an array of strings which is a custom transliteration map. It can replace anything with something else. For example:

```toml
custom = [
    "test", "replaced",
    "æ",    "ae",
    "ē",    "e",
]
```

An array is used because Golang does not preserve the order of a map.

---

### notifs.ignore

#### sender

This field is an array of strings which contains senders from which notifications are ignored.

#### summary

This field is an array of strings which contains notification summaries whose notifications will be ignored.


#### body&nbsp;

This field is an array of strings which contains notification bodies whose notifications will be ignored.

---

### music

This section contains options for music control

#### vol.interval

This field is an integer indicating how much the volume should be changed for each volume up or down event from the music control app.

---

### weather

This section contains options for ITD's weather support

#### weather.enabled

This field is a boolean indicating whether the weather support should be enabled.

#### weather.location

This field is a string. It is given to OpenStreetMap Nominatim to retrieve coordinates to feed into the Norwegian Meteorological Institute's API. Anything that can be found on OpenStreetMap should also work here. It will always use the first result.