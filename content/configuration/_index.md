+++
chapter = true
pre = "<b>II. </b>"
title = "Configuration"
weight = 2
+++

### Chapter II

# Configuration

This chapter is about configuring ITD, including documentation about its config file and locations where it can be put.